create table user
(
    id       bigint primary key auto_increment,
    username varchar(40) not null unique,
    password varchar(40) not null,
    email    varchar(40) not null unique,
    name     varchar(40) not null,
    surname  varchar(40) not null,
    birthday date
);

create table notebook
(
    id          bigint primary key auto_increment,
    name        varchar(255) not null,
    description varchar(1024),
    user_id     bigint       not null
);

create table note
(
    id               bigint primary key auto_increment,
    title            varchar(255)  not null,
    text             varchar(1024) not null,
    last_modified_on date,
    notebook_id      bigint        not null
);


create table to_do
(
    id               bigint primary key auto_increment,
    title            varchar(255) not null,
    description      varchar(1024),
    deadline         date,
    last_modified_on date,
    notebook_id      bigint       not null
);

alter table notebook
    add foreign key (user_id) references user (id);

alter table note
    add foreign key (notebook_id) references notebook (id);

alter table to_do
    add foreign key (notebook_id) references notebook (id);

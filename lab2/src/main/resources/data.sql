insert into user(username, password, email, name, surname, birthday)
values ('johny', '1111', 'johny@email.com', 'John', 'Jackson', '2000-02-02'),
       ('jacky', '1111', 'jacky@email.com', 'Jack', 'Johnson', '2001-01-01');

insert into notebook(name, description, user_id)
values ('lab 2', 'about lab 2', 1),
       ('lab 2', 'about lab 2', 2);


insert into note(title, text, notebook_id)
values ('REST', 'REST - representational state transfer', 1),
       ('MVC', 'MVC - model-view-controller', 2);

insert into to_do(title, deadline, notebook_id)
values ('finish lab 2', '2020-03-03', 1),
       ('finish lab 1', '2020-02-22', 2);

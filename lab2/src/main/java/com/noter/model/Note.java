package com.noter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@Entity
@Table
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(length = 1024)
    private String text;

    @Column
    private Date lastModifiedOn;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "notebook_id", nullable = false)
    private Notebook notebook;

    public Note() {
        this.lastModifiedOn = new Date(System.currentTimeMillis());
    }
}

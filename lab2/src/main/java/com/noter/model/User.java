package com.noter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "username is not present")
    @Size(min = 2, max = 40, message = "inappropriate username length")
    @Column(length = 40, nullable = false, unique = true)
    private String username;

    @NotNull(message = "password is not present")
    @Size(min = 2, max = 40, message = "inappropriate password length")
    @Column(length = 40, nullable = false)
    private String password;

    @NotNull(message = "email is not present")
    @Email(message = "email doesn't match")
    @Column(length = 40, nullable = false, unique = true)
    private String email;

    @NotNull(message = "name is not present")
    @Size(min = 2, max = 40, message = "inappropriate name length")
    @Column(length = 40, nullable = false)
    private String name;

    @NotNull(message = "surname is not present")
    @Size(min = 2, max = 40, message = "inappropriate surname length")
    @Column(length = 40, nullable = false)
    private String surname;

    @DateTimeFormat(pattern="yyyy-mm-dd")
    @Column
    private Date birthday;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Notebook> notebooks;
}

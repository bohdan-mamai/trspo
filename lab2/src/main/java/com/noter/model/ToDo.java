package com.noter.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@Entity
@Table(name = "to_do")
public class ToDo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(length = 1024)
    private String description;

    @Column
    private Date deadline;

    @Column
    private Date lastModifiedOn;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "notebook_id", nullable = false)
    private Notebook notebook;

    public ToDo() {
        this.lastModifiedOn = new Date(System.currentTimeMillis());
    }
}

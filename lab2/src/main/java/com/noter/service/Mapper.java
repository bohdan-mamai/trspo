package com.noter.service;

import com.noter.dto.NoteDTO;
import com.noter.dto.NotebookDTO;
import com.noter.dto.ToDoDTO;
import com.noter.model.Note;
import com.noter.model.Notebook;
import com.noter.model.ToDo;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
@RequiredArgsConstructor
public class Mapper {
    private final ModelMapper modelMapper;

    public NotebookDTO toDTO(Notebook notebook) {
        return modelMapper.map(notebook, NotebookDTO.class);
    }

    public Notebook toModel(NotebookDTO notebookDTO) {
        return modelMapper.map(notebookDTO, Notebook.class);
    }

    public NoteDTO toDTO(Note note) {
        return modelMapper.map(note, NoteDTO.class);
    }

    public Note toModel(NoteDTO noteDTO) {
        Note note = modelMapper.map(noteDTO, Note.class);
        if (noteDTO.getLastModifiedOn() == null) {
            note.setLastModifiedOn(new Date(System.currentTimeMillis()));
        }
        return note;
    }

    public ToDoDTO toDTO(ToDo toDo) {
        return modelMapper.map(toDo, ToDoDTO.class);
    }

    public ToDo toModel(ToDoDTO toDoDTO) {
        ToDo toDo = modelMapper.map(toDoDTO, ToDo.class);
        if (toDoDTO.getLastModifiedOn() == null) {
            toDo.setLastModifiedOn(new Date(System.currentTimeMillis()));
        }
        return toDo;
    }

}

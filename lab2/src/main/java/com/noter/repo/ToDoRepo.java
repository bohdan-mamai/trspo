package com.noter.repo;

import com.noter.model.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ToDoRepo extends JpaRepository<ToDo, Long> {
    List<ToDo> findAllByNotebookId(Long notebookId);
}

package com.noter.repo;

import com.noter.model.Notebook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotebookRepo extends JpaRepository<Notebook, Long> {
    List<Notebook> findAllByUserId(Long userId);
}

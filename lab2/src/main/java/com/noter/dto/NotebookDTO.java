package com.noter.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class NotebookDTO {
    private Long id;

    @NotNull(message = "name is not present")
    @Size(min = 1, max = 255, message = "inappropriate name length")
    private String name;

    @Size(max = 1024, message = "Description too long")
    private String description;

    @NotNull(message = "user id not specified")
    private Long userId;
}

package com.noter.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.sql.Date;

@Getter
@Setter
public class ToDoDTO {
    private Long id;

    @NotNull(message = "title is not present")
    @Size(min = 1, max = 255, message = "inappropriate title length")
    private String title;

    @Size(max = 1024, message = "Text too long")
    private String description;

    @DateTimeFormat(pattern="yyyy-mm-dd")
    private Date deadline;

    @DateTimeFormat(pattern="yyyy-mm-dd")
    private Date lastModifiedOn;

    @NotNull(message = "notebook id not specified")
    private Long notebookId;
}

package com.noter.controller;

import com.noter.dto.ToDoDTO;
import com.noter.model.ToDo;
import com.noter.repo.ToDoRepo;
import com.noter.service.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/noter/todo")
@RequiredArgsConstructor
public class ToDoController {
    private final ToDoRepo toDoRepo;
    private final Mapper mapper;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ToDoDTO>> getAllToDo() {
        List<ToDoDTO> toDoList = toDoRepo.findAll().stream().map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(toDoList);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ToDoDTO> getToDo(@PathVariable("id") Long toDoId) {
        return toDoRepo.findById(toDoId)
                .map(toDo -> ResponseEntity.ok(mapper.toDTO(toDo)))
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> deleteToDo(@PathVariable("id") Long toDoId) {
        Optional<ToDo> toDoOptional = toDoRepo.findById(toDoId);
        if (toDoOptional.isPresent()) {
            toDoRepo.delete(toDoOptional.get());
            return ResponseEntity.ok("ToDo successfully deleted");
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> createToDo(@RequestBody @Valid ToDoDTO toDoDTO) {
        if (toDoDTO.getId() != null && toDoRepo.findById(toDoDTO.getId()).isPresent()) {
            return new ResponseEntity<>("ToDo already exists", HttpStatus.CONFLICT);
        }
        ToDo toDo = mapper.toModel(toDoDTO);
        toDoRepo.save(toDo);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(toDo.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> updateToDo(@PathVariable("id") Long toDoId, @RequestBody @Valid ToDoDTO toDoDTO) {
        if (!toDoRepo.findById(toDoId).isPresent()) {
            return ResponseEntity.notFound().build();
        }
        toDoDTO.setId(toDoId);
        toDoRepo.save(mapper.toModel(toDoDTO));
        return ResponseEntity.ok("ToDo successfully updated");
    }

    @GetMapping("/notebook/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ToDoDTO>> getToDoListFromNotebook(@PathVariable("id") Long notebookId) {
        List<ToDoDTO> toDoList = toDoRepo.findAllByNotebookId(notebookId).stream()
                .map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(toDoList);
    }
}

package com.noter.controller;

import com.noter.dto.NoteDTO;
import com.noter.model.Note;
import com.noter.repo.NoteRepo;
import com.noter.service.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/noter/notes")
@RequiredArgsConstructor
public class NoteController {
    private final NoteRepo noteRepo;
    private final Mapper mapper;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<NoteDTO>> getAllNotes() {
        List<NoteDTO> notes = noteRepo.findAll().stream().map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(notes);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<NoteDTO> getNote(@PathVariable("id") Long noteId) {
        return noteRepo.findById(noteId)
                .map(note -> ResponseEntity.ok(mapper.toDTO(note)))
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> deleteNote(@PathVariable("id") Long noteId) {
        Optional<Note> noteToBeDeleted = noteRepo.findById(noteId);
        if (noteToBeDeleted.isPresent()) {
            noteRepo.delete(noteToBeDeleted.get());
            return ResponseEntity.ok("Note successfully deleted");
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> createNote(@RequestBody @Valid NoteDTO noteDTO) {
        if (noteDTO.getId() != null && noteRepo.findById(noteDTO.getId()).isPresent()) {
            return new ResponseEntity<>("Note already exists", HttpStatus.CONFLICT);
        }
        Note note = mapper.toModel(noteDTO);
        noteRepo.save(note);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(note.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> updateNote(@PathVariable("id") Long noteId, @RequestBody @Valid NoteDTO noteDTO) {
        if (!noteRepo.findById(noteId).isPresent()) {
            return ResponseEntity.notFound().build();
        }
        noteDTO.setId(noteId);
        noteRepo.save(mapper.toModel(noteDTO));
        return ResponseEntity.ok("Note successfully updated");
    }

    @GetMapping("/notebook/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<NoteDTO>> getNotesFromNotebook(@PathVariable("id") Long notebookId) {
        List<NoteDTO> notes = noteRepo.findAllByNotebookId(notebookId).stream()
                .map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(notes);
    }
}

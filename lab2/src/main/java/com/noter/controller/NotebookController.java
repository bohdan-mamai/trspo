package com.noter.controller;

import com.noter.dto.NotebookDTO;
import com.noter.model.Notebook;
import com.noter.repo.NotebookRepo;
import com.noter.service.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/noter/notebooks")
@RequiredArgsConstructor
public class NotebookController {
    private final NotebookRepo notebookRepo;
    private final Mapper mapper;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<NotebookDTO>> getAllNotebooks() {
        List<NotebookDTO> notebooks = notebookRepo.findAll().stream()
                .map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(notebooks);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<NotebookDTO> getNotebook(@PathVariable("id") Long notebookId) {
        return notebookRepo.findById(notebookId)
                .map(notebook -> ResponseEntity.ok(mapper.toDTO(notebook)))
                .orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> deleteNotebook(@PathVariable("id") Long notebookId) {
        Optional<Notebook> notebookToBeDeleted = notebookRepo.findById(notebookId);
        if (notebookToBeDeleted.isPresent()) {
            notebookRepo.delete(notebookToBeDeleted.get());
            return ResponseEntity.ok("Notebook successfully deleted");
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> createNotebook(@RequestBody @Valid NotebookDTO notebookDTO) {
        if (notebookDTO.getId() != null && notebookRepo.findById(notebookDTO.getId()).isPresent()) {
            return new ResponseEntity<>("Notebook already exists", HttpStatus.CONFLICT);
        }
        Notebook notebook = mapper.toModel(notebookDTO);
        notebookRepo.save(notebook);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(notebook.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> updateNotebook(@PathVariable("id") Long notebookId, @RequestBody @Valid NotebookDTO notebookDTO) {
        if (!notebookRepo.findById(notebookId).isPresent()) {
            return ResponseEntity.notFound().build();
        }
        notebookDTO.setId(notebookId);
        notebookRepo.save(mapper.toModel(notebookDTO));
        return ResponseEntity.ok("Notebook successfully updated");
    }

    @GetMapping("/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<NotebookDTO>> getUserNotebooks(@PathVariable("id") Long userId) {
        List<NotebookDTO> notebooks = notebookRepo.findAllByUserId(userId).stream()
                .map(mapper::toDTO).collect(Collectors.toList());
        return ResponseEntity.ok(notebooks);
    }
}

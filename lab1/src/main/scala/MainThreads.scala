
import java.awt.Polygon
import java.io.{BufferedWriter, FileWriter}


object MainThreads extends App {

  val ITERATION = 1000
  val RANGE_MIN = -100
  val RANGE_MAX = 100
  val func = new Functions()
  val polygon = new Polygon()
  val start_time = System.currentTimeMillis()

  val points = func.generatePoints(10, RANGE_MAX, RANGE_MIN)


  val hull = func.
    convexHullThreads(points)

  hull.foreach(p => polygon.addPoint(p.getX.floor.toInt, p.getY.floor.toInt))
  val file = "pointsThr.txt"
  val writer = new BufferedWriter(new FileWriter(file))

  for (_ <- 0 to ITERATION) {

    val points2 = func.generatePoints(3, RANGE_MAX, RANGE_MIN)
    val rez = func.checkList(polygon, points2)
    writer.write(points2.toString() + "\n")
    writer.write(rez.toString + "\n")
  }

  val end_time = (System.currentTimeMillis() - start_time).toDouble / 1000

  println("Threads time: " + end_time)
}



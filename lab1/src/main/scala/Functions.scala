import java.awt.Polygon
import java.util.concurrent.{Callable, ExecutorService, Executors}
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class Functions {

  def convexHull(points: ListBuffer[Point]): ListBuffer[Point] = {

    def cross(o: Point, a: Point, b: Point): Double = {
      (a.getX - o.getX) * (b.getY - o.getY) - (a.getY - o.getY) * (b.getX - o.getX)
    }

    val distinctPoints = points.distinct

    if (distinctPoints.length < 2) {
      points
    } else {
      val sortedPoints = distinctPoints.sortBy(f => f.getX)

      val lower = ListBuffer[Point]()
      for (i <- sortedPoints) {
        while (lower.length >= 2 && cross(lower(lower.length - 2), lower.last, i) <= 0) {
          lower -= lower.last
        }
        lower += i
      }

      val upper = ArrayBuffer[Point]()
      for (i <- sortedPoints.reverse) {
        while (upper.size >= 2 && cross(upper(upper.length - 2), upper(upper.length - 1), i) <= 0) {
          upper -= upper.last
        }
        upper += i
      }

      lower -= lower.last
      upper -= upper.last
      lower ++= upper
    }

  }

  def generatePoints(capacity: Int, rangeMax: Int, rangeMin: Int): ListBuffer[Point] = {
    val points = ListBuffer[Point]()
    val random = new Random()
    for (_ <- 1 to capacity) {
      points += new Point(rangeMin + (rangeMax - rangeMin) * random.nextDouble(),
        rangeMin + (rangeMax - rangeMin) * random.nextDouble())

    }
    points
  }

  def checkList(polygon: Polygon, points: ListBuffer[Point]): Boolean = {
    val rez = {
      points
        .map(p => polygon.contains(p.getX, p.getY))
        .forall(_ == false)
    }
    rez
  }

  def convexHullThreads(points: ListBuffer[Point]): ListBuffer[Point] = {

    def cross(o: Point, a: Point, b: Point): Double = {
      (a.getX - o.getX) * (b.getY - o.getY) - (a.getY - o.getY) * (b.getX - o.getX)
    }

    val sortedPoints = points.sortBy(f => f.getX)


    val pool: ExecutorService = Executors.newCachedThreadPool()

    val future: Callable[ListBuffer[Point]] = new Callable[ListBuffer[Point]]() {
      def call(): ListBuffer[Point] = {
        val lower = ListBuffer[Point]()
        for (i <- sortedPoints) {
          while (lower.length >= 2 && cross(lower(lower.length - 2), lower.last, i) <= 0) {
            lower -= lower.last
          }
          lower += i
        }
        lower
      }
    }

    val future2: Callable[ListBuffer[Point]] = new Callable[ListBuffer[Point]]() {
      def call(): ListBuffer[Point] = {
        val upper = ListBuffer[Point]()
        for (i <- sortedPoints.reverse) {
          while (upper.size >= 2 && cross(upper(upper.length - 2), upper.last, i) <= 0) {
            upper -= upper.last
          }
          upper += i
        }
        upper
      }
    }

    val lower = pool.submit(future).get()
    val upper = pool.submit(future2).get()
    pool.shutdownNow()


    lower ++= upper
  }


}

package com.lab.sharingProject.feign;

import com.lab.sharingProject.feign.dto.ClientDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "clientService", url = "${clientService.connect.url}")
@Component
public interface ClientServiceClient {

    @GetMapping("api/users/")
    List<ClientDto> getAll();

    @GetMapping("api/users/{id}")
    ClientDto getById(@PathVariable int id);

    @PostMapping("api/users/")
    ClientDto createUser(@RequestBody ClientDto client);

    @PutMapping("api/users/")
    ClientDto updateUser(@RequestBody ClientDto client, @RequestParam int id);

    @DeleteMapping("api/users/{id}")
    void deleteUser(@PathVariable int id);

    @PatchMapping("api/users/paying")
    double updateFunds(@RequestParam int clientId, @RequestParam double money);
}

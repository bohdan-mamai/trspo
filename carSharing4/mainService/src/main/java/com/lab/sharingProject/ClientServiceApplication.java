package com.lab.sharingProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
public class ClientServiceApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(ClientServiceApplication.class, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package com.lab.sharingProject.service;

import com.lab.sharingProject.dao.CarDao;
import com.lab.sharingProject.dao.OrderDao;
import com.lab.sharingProject.exceptions.CarAlreadyInUseException;
import com.lab.sharingProject.exceptions.EntityNotFoundException;
import com.lab.sharingProject.exceptions.NotEnoughMoney;
import com.lab.sharingProject.feign.ClientServiceClient;
import com.lab.sharingProject.feign.dto.ClientDto;
import com.lab.sharingProject.model.Car;
import com.lab.sharingProject.model.Order;
import com.lab.sharingProject.dto.OrderDto;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderDao orderDao;
    private final ClientServiceClient clientSC;
    private final CarDao carDao;
    private final ModelMapper mapper;

    public OrderDto open(int clientId, int carId, int time) {
        ClientDto client;
        try {
            client = clientSC.getById(clientId);
        } catch (FeignException.FeignClientException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        Car car = carDao.getById(carId);
        if (car == null) {
            throw new EntityNotFoundException("Car not found: id=" + carId);
        }
        Order order = Order.builder()
                .car(car)
                .clientId(clientId)
                .sharingTime(time)
                .build();
        double price = getPrice(car, order.getSharingTime());
        if (client.getFunds() < price) {
            throw new NotEnoughMoney("Please top up your account");
        }
        if (car.isInSharing()) {
            throw new CarAlreadyInUseException("Please choose another car");
        }
        client.setFunds(client.getFunds() - price);
        clientSC.updateUser(client, clientId);
        car.setInSharing(true);
        carDao.update(car);

        return mapper.map(orderDao.save(order), OrderDto.class);
    }

    public OrderDto update(OrderDto order, int id) {
        Order newOrder = mapper.map(order, Order.class);
        newOrder.setId(id);
        return mapper.map(orderDao.update(newOrder), OrderDto.class);
    }

    public void delete(int id) {
        Order order = orderDao.getById(id);
        Car car = order.getCar();
        int clientId = order.getClientId();
        ClientDto client = clientSC.getById(clientId);
        if (client.getFunds() < car.getCrashLevel() * 1000) {
            throw new NotEnoughMoney("Please top up your account and try again");
        }
        client.setFunds(client.getFunds() - car.getCrashLevel() * 1000);
        car.setInSharing(false);
        car.setCrashLevel(0);
        carDao.update(car);
        orderDao.delete(orderDao.getById(id));
    }

    public OrderDto getById(int id) {
        Order order = orderDao.getById(id);
        if (order == null) {
            throw new EntityNotFoundException("addressExceptionMessage" + id);
        }
        return mapper.map(order, OrderDto.class);
    }

    public List<OrderDto> getAll() {
        return orderDao
                .getAll()
                .stream()
                .map(order -> mapper.map(order, OrderDto.class))
                .collect(Collectors.toList());
    }

    private double getPrice(Car car, int sharingTime) {
        double price = car.getEngineVolume() * sharingTime * 100;
        log.info("Order price: " + price);
        return price;
    }
}

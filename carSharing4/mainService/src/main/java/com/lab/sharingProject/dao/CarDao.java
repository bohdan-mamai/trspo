package com.lab.sharingProject.dao;

import com.lab.sharingProject.model.Car;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
@Slf4j
public class CarDao implements Crud<Car> {

    @PersistenceContext
    private EntityManager manager;

    @Autowired
    OrderDao orderDao;

    @Override
    public Car save(Car newCar) {
        log.info("Saving Car: {}", newCar);
        manager.persist(newCar);
        return newCar;
    }

    @Override
    public Car getById(int id) {
        log.info("Getting Car by id: {}", id);
        return manager.find(Car.class, id);
    }

    @Override
    public Car update(Car updatedCar) {
        log.info("Updating Car id: {}", updatedCar.getId());
        return manager.merge(updatedCar);
    }

    @Override
    public void delete(Car deletedCar) {
        log.info("Deleting Car by id: {}", deletedCar.getId());
        //orderDao.deleteByBook(deletedCar.getId());
        manager.remove(deletedCar);
      //  manager.createQuery("delete from book where book_id=2");
    }

    @Override
    public List<Car> getAll() {
        log.info("Getting List of Cars");
        return manager.createQuery("SELECT c FROM Car c", Car.class)
                .getResultList();
    }
}

package com.lab.sharingProject.exceptions;

public class CarAlreadyInUseException extends RuntimeException {

    public CarAlreadyInUseException(String message) {
        super(message);
    }

}


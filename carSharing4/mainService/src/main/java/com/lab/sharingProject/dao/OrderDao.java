package com.lab.sharingProject.dao;

import com.lab.sharingProject.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
@Slf4j
public class OrderDao implements Crud<Order> {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Order save(Order newOrder) {
        log.info("Saving Book: {}", newOrder);
        manager.persist(newOrder);
        return newOrder;
    }

    @Override
    public Order getById(int id) {
        log.info("Getting Order by id: {}", id);
        return manager.find(Order.class, id);
    }

    @Override
    public Order update(Order order) {
        log.info("Updating Book id: {}", order.getId());
        return manager.merge(order);
    }

    @Override
    public void delete(Order order) {
        log.info("Deleting Book by id: {}", order.getId());
        manager.remove(order);
    }

    public void deleteByCar(int carId) {
        manager.createQuery("SELECT c FROM Order c WHERE c.carId= :carId", Order.class)
                .setParameter("carId", carId)
                .getResultStream()
                .forEach(order -> delete(order));
    }

    @Override
    public List<Order> getAll() {
        log.info("Getting List of Orders");
        return manager.createQuery("SELECT c FROM Order c", Order.class)
                .getResultList();
    }


//    public List<User> getUsersInHoll() {
//        return getOrdersInHoll().stream()
//                .map(order -> manager.find(User.class, order.getUser_id()))
//                .distinct()
//                .collect(Collectors.toList());
//    }
//
//    public List<User> getUsersWithAbonements() {
//        return getOrdersWithAbonement().stream()
//                .map(order -> manager.find(User.class, order.getUser_id()))
//                .collect(Collectors.toList());
//    }
//
//    public List<Order> getOrdersInHoll() {
//
////        String orderType = "READING_HOLL";
//       // OrderType orderType = OrderType.READING_HOLL;
//        return manager.createQuery("SELECT c FROM Order c WHERE c.orderType = :orderType", Order.class)
//                .setParameter("orderType", orderType)
//                .getResultList();
//
//    }
//
//    public List<Order> getOrdersWithAbonement() {
//
//        OrderType orderType = OrderType.ABONEMENT;
////        String orderType = "ABONEMENT";
//        return manager.createQuery("SELECT c FROM Order c WHERE c.orderType = :orderType", Order.class)
//                .setParameter("orderType", orderType)
//                .getResultList();
//
//    }
}

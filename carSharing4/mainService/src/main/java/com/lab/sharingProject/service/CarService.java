package com.lab.sharingProject.service;

import com.lab.sharingProject.dao.CarDao;
import com.lab.sharingProject.exceptions.EntityNotFoundException;
import com.lab.sharingProject.model.Car;
import com.lab.sharingProject.dto.CarDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CarService {

    private final CarDao carDao;
    private final ModelMapper mapper;

    public CarDto save(CarDto car) {
        Car newCar = mapper.map(car, Car.class);
        return mapper.map(carDao.save(newCar), CarDto.class);
    }

    public CarDto update(CarDto car, int id) {
        Car newCar = mapper.map(car, Car.class);
        newCar.setId(id);
        return mapper.map(carDao.update(newCar), CarDto.class);
    }

    public void delete(int id) {
        carDao.delete(carDao.getById(id));
    }

    public CarDto getById(int id) {
        Car car = carDao.getById(id);
        if (car == null) {
            throw new EntityNotFoundException("addressExceptionMessage" + id);
        }
        return mapper.map(car, CarDto.class);
    }

    public List<CarDto> getAll() {
        return carDao
                .getAll()
                .stream()
                .map(car -> mapper.map(car, CarDto.class))
                .collect(Collectors.toList());
    }

    public List<CarDto> getFreeCars() {
        return carDao
                .getAll()
                .stream()
                .filter(car -> !car.isInSharing())
                .map(car -> mapper.map(car, CarDto.class))
                .collect(Collectors.toList());
    }

}

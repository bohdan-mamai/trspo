package com.lab.sharingProject.service;

import com.lab.sharingProject.dao.ClientDao;
import com.lab.sharingProject.exceptions.EntityNotFoundException;
import com.lab.sharingProject.model.Client;
import com.lab.sharingProject.dto.ClientDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientService {

    private final ClientDao clientDao;
    private final ModelMapper mapper;

    public ClientDto save(ClientDto client) {
        Client newClient = mapper.map(client, Client.class);
        return mapper.map(clientDao.save(newClient), ClientDto.class);
    }

    public ClientDto update(ClientDto client, int id) {
        Client newClient = mapper.map(client, Client.class);
        newClient.setId(id);
        return mapper.map(clientDao.update(newClient), ClientDto.class);
    }

    public void delete(int id) {
        clientDao.delete(clientDao.getById(id));
    }

    public ClientDto getById(int id) {
        Client client = clientDao.getById(id);
        if (client == null) {
            throw new EntityNotFoundException("ClientExceptionMessage: id=" + id);
        }
        return mapper.map(client, ClientDto.class);
    }

    public List<ClientDto> getAll() {
        return clientDao
                .getAll()
                .stream()
                .map(client -> mapper.map(client, ClientDto.class))
                .collect(Collectors.toList());
    }

    public double updateFunds(int clientId, double money) {
        Client client = clientDao.getById(clientId);
        client.setFunds(client.getFunds() + money);
        clientDao.update(client);
        return client.getFunds();
    }
}


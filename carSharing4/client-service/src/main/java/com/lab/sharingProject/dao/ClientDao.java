package com.lab.sharingProject.dao;

import com.lab.sharingProject.model.Client;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
@Slf4j
public class ClientDao implements Crud<Client> {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Client save(Client newClient) {
        log.info("Saving Client: {}", newClient);
        manager.persist(newClient);
        return newClient;
    }

    @Override
    public Client getById(int id) {
        log.info("Getting User by id: {}", id);
        return manager.find(Client.class, id);
    }

    @Override
    public Client update(Client client) {
        log.info("Updating Coach id: {}", client.getId());
        return manager.merge(client);
    }

    @Override
    public void delete(Client client) {
        log.info("Deleting Coach id: {}", client.getId());
       // manager.createQuery("delete from user_ where user_id=" + user.getId());
        manager.remove(client);

    }

    @Override
    public List<Client> getAll() {
        log.info("Getting List of Users");
        return manager.createQuery("SELECT u FROM Client u", Client.class)
                .getResultList();
    }

}

package com.lab.sharingProject.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phonenumber")
    private String phoneNumber;

    @Column(name = "passport_number")
    private String passportNumber;

    @Column(name = "funds")
    private double funds;

    @Column(name = "order_id")
    private int orderId;

}


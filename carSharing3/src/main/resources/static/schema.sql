
CREATE TABLE cars
(
    id              INT AUTO_INCREMENT PRIMARY KEY,
    brand           VARCHAR(30)			NOT NULL,
    engine_volume	DECIMAL     		NOT NULL
);
INSERT INTO cars(brand, engine_volume)
VALUES ('ZAZ Lanos', 1.2),
       ('Daewoo Nubira', 1.6),
       ('Shkoda Octavia A7', 2.0);

CREATE TABLE client
(
    id                       INT AUTO_INCREMENT PRIMARY KEY,
    first_name               VARCHAR(20)        NOT NULL,
    last_name                VARCHAR(20)        NOT NULL,
    passport_number          VARCHAR(20)        NOT NULL UNIQUE,
    phonenumber              VARCHAR(11)        UNIQUE
);

INSERT INTO client (first_name, last_name, passport_number, phonenumber)
VALUES ('nazar', 'koval', '87346483742', '0679359820'),
       ('yura', 'khanas', '89467834164','9843439249'),
       ('yarek', 'teplyy', '89426434093', '2323232324');


CREATE TABLE orders
(
    id               INT AUTO_INCREMENT PRIMARY KEY,
    sharing_time	 INT                 NOT NULL,
    client_id	   	 INT,
    car_id	   	     INT,

    CONSTRAINT fk_order_client
        FOREIGN KEY (client_id)
            REFERENCES client (id)
--     CONSTRAINT fk_order_car
--         FOREIGN KEY (car_id)
--             REFERENCES cars (id)
);
INSERT INTO orders(sharing_time, client_id, car_id)
VALUES (10, 1, 1),
       (8, 2, 2),
       (8, 3, 3);






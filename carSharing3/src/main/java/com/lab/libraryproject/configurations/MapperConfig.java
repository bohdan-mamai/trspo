package com.lab.libraryproject.configurations;


import com.lab.libraryproject.dto.OrderDto;
import com.lab.libraryproject.mappers.EventDtoMapper;
import com.lab.libraryproject.mappers.OrderMapper;
import com.lab.libraryproject.model.Order;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class MapperConfig {

    private final OrderMapper orderMapper;
    private final EventDtoMapper eventDtoMapper;

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

//        modelMapper
//                .createTypeMap(OrderDto.class, Order.class)
//                .addMappings(eventDtoMapper.toModel);
//
//        modelMapper.getConfiguration().setAmbiguityIgnored(true);
////        modelMapper
////                .createTypeMap(OrderDto.class, Order.class)
////                .addMappings(orderMapper);
       return modelMapper;
    }
}

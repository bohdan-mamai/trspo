package com.lab.libraryproject.service;

import com.lab.libraryproject.dao.CarDao;
import com.lab.libraryproject.dao.ClientDao;
import com.lab.libraryproject.dao.OrderDao;
import com.lab.libraryproject.dto.CarDto;
import com.lab.libraryproject.exceptions.CarAlreadyInUseException;
import com.lab.libraryproject.exceptions.NotEnoughMoney;
import com.lab.libraryproject.model.Car;
import com.lab.libraryproject.model.Client;
import com.lab.libraryproject.model.Order;
import com.lab.libraryproject.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderDao orderDao;
    private final ClientDao clientDao;
    private final CarDao carDao;
    private final ModelMapper mapper;

    public OrderDto open(int clientId, int carId, int time) {
        Client client = clientDao.getById(clientId);
        Car car = carDao.getById(carId);
        Order order = Order.builder()
                .car(car)
                .client(client)
                .sharingTime(time)
                .build();
        double price = getPrice(car, order.getSharingTime());
        if (client.getFunds() < price) {
            throw new NotEnoughMoney("Please top up your account");
        }
        if (car.isInSharing()) {
            throw new CarAlreadyInUseException("Please choose another car");
        }
        client.setFunds(client.getFunds() - price);
        clientDao.update(client);
        car.setInSharing(true);
        carDao.update(car);

        return mapper.map(orderDao.save(order), OrderDto.class);
    }

    public OrderDto update(OrderDto order, int id) {
        Order newOrder = mapper.map(order, Order.class);
        newOrder.setId(id);
        return mapper.map(orderDao.update(newOrder), OrderDto.class);
    }

    public void delete(int id) {
        Order order = orderDao.getById(id);
        Car car = order.getCar();
        Client client = order.getClient();
        if (client.getFunds() < car.getCrashLevel() * 1000) {
            throw new NotEnoughMoney("Please top up your account and try again");
        }
        client.setFunds(client.getFunds() - car.getCrashLevel() * 1000);
        car.setInSharing(false);
        car.setCrashLevel(0);
        carDao.update(car);
        orderDao.delete(orderDao.getById(id));
    }

    public OrderDto getById(int id) {
        Order order = orderDao.getById(id);
        if (order == null) {
            throw new EntityNotFoundException("addressExceptionMessage" + id);
        }
        return mapper.map(order, OrderDto.class);
    }

    public List<OrderDto> getAll() {
        return orderDao
                .getAll()
                .stream()
                .map(order -> mapper.map(order, OrderDto.class))
                .collect(Collectors.toList());
    }

    private double getPrice(Car car, int sharingTime) {
        double price = car.getEngineVolume() * sharingTime * 100;
        log.info("Order price: " + price);
        return price;
    }
}

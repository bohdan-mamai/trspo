package com.lab.libraryproject.controller;

import com.lab.libraryproject.dto.OrderDto;
import com.lab.libraryproject.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("api/orders")
public class OrderController {

    private final OrderService orderService;

    @PostMapping("/open")
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDto openOrder(@RequestParam int clientId, int carId, int time) {
        return orderService.open( clientId, carId,  time);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable int id) {
        orderService.delete(id);
    }

    @GetMapping("/")
    public List<OrderDto> getAll() {
        List<OrderDto> list = orderService.getAll();
        return list;
    }


}

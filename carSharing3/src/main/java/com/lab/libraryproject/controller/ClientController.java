package com.lab.libraryproject.controller;

import com.lab.libraryproject.service.ClientService;
import com.lab.libraryproject.dto.ClientDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/users")
public class ClientController {

    private final ClientService clientService;

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public List<ClientDto> getAll() {
        return clientService.getAll();
    }

    @GetMapping("/{id}")
    public ClientDto getById(@PathVariable int id) {
        return clientService.getById(id);
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public ClientDto createUser(@RequestBody ClientDto client) {
        return clientService.save(client);
    }

    @PutMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public ClientDto updateUser(@RequestBody ClientDto client, @RequestParam int id) {
        return clientService.update(client, id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable int id) {
        clientService.delete(id);
    }

    @PatchMapping("/paying")
    @ResponseStatus(HttpStatus.OK)
    public double updateFunds(@RequestParam int clientId, @RequestParam double money){
       return clientService.updateFunds(clientId, money);
    }
}

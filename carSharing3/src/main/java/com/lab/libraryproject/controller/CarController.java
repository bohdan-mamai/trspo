package com.lab.libraryproject.controller;

import com.lab.libraryproject.service.CarService;
import com.lab.libraryproject.dto.CarDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("api/cars")
public class CarController {

    private final CarService carService;

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public List<CarDto> getAll(){
        return carService.getAll();
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public CarDto createCar(@RequestBody CarDto car) {
        return carService.save(car);
    }

    @PutMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public CarDto updateCar(@RequestBody CarDto car, @RequestParam int id) {
        return carService.update(car, id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCar(@PathVariable int id) {
        carService.delete(id);
    }

    @GetMapping("/free")
    @ResponseStatus(HttpStatus.OK)
    public List<CarDto> getFreeCars(){
        return carService.getFreeCars();
    }

}

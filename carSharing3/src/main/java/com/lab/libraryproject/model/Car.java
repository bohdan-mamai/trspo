package com.lab.libraryproject.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "brand")
    private String brand;
    @Column(name = "engine_volume")
    private double engineVolume;
    @Column(name = "in_sharing")
    private boolean inSharing;
    @Column(name = "crash_level")
    private int crashLevel;

    @OneToOne(mappedBy = "car", fetch = FetchType.EAGER)
    private Order order;

}

package com.lab.libraryproject.mappers;

import com.lab.libraryproject.dto.OrderDto;
import com.lab.libraryproject.model.Order;
import lombok.RequiredArgsConstructor;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrderMapper extends PropertyMap<OrderDto, Order> {

    @Override
    protected void configure() {
        skip( destination.getId());
        map(source.getCarId(), destination.getCar().getId());
        map(source.getClientId(), destination.getClient().getId());
    }
}

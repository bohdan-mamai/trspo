package com.lab.libraryproject.mappers;


import com.lab.libraryproject.dto.OrderDto;
import com.lab.libraryproject.model.Order;
import lombok.RequiredArgsConstructor;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class EventDtoMapper {

    public PropertyMap<OrderDto, Order> toModel = new PropertyMap<OrderDto, Order>() {

        @Override
        protected void configure() {
            skip(destination.getCar());
            skip(destination.getId());
            skip(destination.getClient());
        }

    };

}

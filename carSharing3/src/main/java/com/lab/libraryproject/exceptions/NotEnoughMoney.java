package com.lab.libraryproject.exceptions;

public class NotEnoughMoney extends RuntimeException {

    public NotEnoughMoney(String message) {
        super(message);
    }
}

package com.lab.libraryproject.exceptions;

public class CarAlreadyInUseException extends RuntimeException {

    public CarAlreadyInUseException(String message) {
        super(message);
    }

}

